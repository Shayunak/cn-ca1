#ifndef CONNECTION_MANAGER_H
#define CONNECTION_MANAGER_H

#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h> 
#include<iostream>
#include "user.h"

#define MAX_LISTEN_Q 20

class connection_manager{
public:
    connection_manager(int _port, int _last_data_channel_port);
    void run();
private:
    //fields
    int port;
    int listening_sock_fd;
    int last_data_channel_port;
    //
    int next_data_channel_port();
    void binding(struct sockaddr_in* myAddr);
    void listening();
    void create_socket();
    static void* create_new_user(void* new_user);
    void initialize_server();
};

#endif