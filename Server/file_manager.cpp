#include "file_manager.h"

using namespace std;

file_manager* file_manager::get_instance(){
    if (instance == nullptr)
        instance = new file_manager();
    return instance;
}

string file_manager::get_current_dir(){
    char buffer[MAX_SIZE];
    getcwd(buffer, sizeof(buffer));
    return buffer;
}

file_manager::file_manager(){
    this->extract_help_file();
}

void file_manager::extract_help_file(){
    ifstream help_file("help.txt");
    this->help_text.assign(istreambuf_iterator<char>(help_file), istreambuf_iterator<char>() );
}

string file_manager::get_help(){
    return this->help_text;
}

void file_manager::make_directory(string current_directory, string new_directory){
    string str = current_directory + "/" + new_directory;
    if(mkdir(&str[0], 0777) == -1)
        throw other_errors_exception();
}

void file_manager::delete_file(string current_directory, string file_name){
    string str = current_directory + "/" + file_name;
    if(remove(&str[0]) == -1)
        throw other_errors_exception();
}

void file_manager::delete_directory(string current_directory, string directory_path){
    string str = current_directory + directory_path;
    if(remove(&str[0]) == -1)
        throw other_errors_exception();
}

void file_manager::change_directory(string current_directory, string path){
    string str = current_directory + path;
    struct stat st;
    if(stat(&str[0], &st) != 0)
        throw other_errors_exception();
}

void file_manager::rename_file(string current_directory, string new_name, string old_name){
    string new_file_name = current_directory + "/" + new_name;
    string old_file_name = current_directory + "/" + old_name;
    if(rename(&old_file_name[0], &new_file_name[0]) == -1)
        throw other_errors_exception();
}

string file_manager::ls(string current_directory){

    // cout << current_directory << endl;

    string str = "";
    struct dirent *de;
    DIR *dr = opendir(&current_directory[0]);
  
    if(dr == NULL)
        throw other_errors_exception();

    while((de = readdir(dr)) != NULL)
        str = str + de->d_name;
  
    closedir(dr);    
    
    // cout << str << endl;
    return str;
}