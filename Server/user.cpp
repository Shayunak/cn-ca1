#include "user.h"

using namespace std;

user::user(int _command_channel, int _data_channel_port){
    this->command_channel = _command_channel;
    this->data_channel_port = _data_channel_port;
    this->is_admin = false;
}

void user::start_user(){
    file_manager* Manager = file_manager::get_instance();
    this->current_state = connected;
    this->current_directory = Manager->get_current_dir();
    command_handler* Handler = command_handler::get_instance();

    send(this->command_channel, &(to_string(this->data_channel_port))[0], MAX_PCKT_SIZE, 0);
    
    char command[MAX_PCKT_SIZE];
    while(true){
        memset(command, 0, MAX_PCKT_SIZE);
        recv(this->command_channel, command, MAX_PCKT_SIZE, 0);
        try{
            Handler->run_command(string(command), this);
        }catch(const exception& e){
            send(this->command_channel, &e.what()[0], MAX_PCKT_SIZE, 0);
        }
    }
}

void user::check_login(){
    if (this->current_state != authenticated)
        throw need_account_for_login_exception();
    return;
}

void user::pwd(){
    check_login();
    string response = "257: " + this->current_directory;
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

string user::get_directory(){
    return this->current_directory;
}

void user::make_directory(string new_directory){
    check_login();
    log* lg = log::get_instance();
    file_manager* Manager = file_manager::get_instance();
    Manager->make_directory(this->current_directory, new_directory);
    lg->create_file_log(this->username, new_directory);
    string response = "257: " + new_directory + " created.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::delete_file(string file_name){
    check_login();
    file_manager* Manager = file_manager::get_instance();
    log* lg = log::get_instance();
    Manager->delete_file(this->current_directory, file_name);
    lg->delete_file_log(this->username, file_name);
    string response = "250: " + file_name + " deleted.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::delete_directory(string directory_path){
    check_login();
    file_manager* Manager = file_manager::get_instance();
    log* lg = log::get_instance();
    Manager->delete_directory(this->current_directory, directory_path);
    lg->delete_file_log(this->username, directory_path);
    string response = "250: " + directory_path + " deleted.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::send_udp(std::string message){
    struct sockaddr_in myAddr;
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(this->data_channel_port);
    myAddr.sin_addr.s_addr = inet_addr(DEFAULT_IP);
    int addrLen = sizeof(myAddr);
    
    sendto(this->data_channel, &message[0], MAX_PCKT_SIZE, 0, (struct sockaddr*) &myAddr, addrLen);
}

void user::ls(){
    check_login();
    file_manager* Manager = file_manager::get_instance();
    send_udp(Manager->ls(this->current_directory) );
    string response = "226: List transfer done.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}



void user::change_directory(string path){
    check_login();
    file_manager* Manager = file_manager::get_instance();
    Manager->change_directory(this->current_directory, path);
    this->current_directory = current_directory + path;
    string response = "250: Successful change.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::rename_file(string old_name, string new_name){
    check_login();
    file_manager* Manager = file_manager::get_instance();
    Manager->rename_file(this->current_directory, new_name, old_name);
    log* lg = log::get_instance();
    lg->rename_file_log(this->username, old_name, new_name);
    string response = "250: Successful change.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::download_file(string file_name){
    check_login();
}

void user::get_help(){
    file_manager* Manager = file_manager::get_instance();
    send(this->command_channel, &(Manager->get_help())[0], MAX_PCKT_SIZE, 0);
}

void user::quit(){
    check_login();
    log* lg = log::get_instance();
    lg->logout_log(this->username);
    this->username = "";
    this->password = "";
    this->current_state = connected;
    this->is_admin = false;
    this->remaining_data = 0;
    string response = "221: Successful Quit.";
    send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
}

void user::login_step1(string username){
    if (this->current_state == connected /* && chechk user name exists in json config*/)
    {
        this->current_state = user_entered;
        this->username = username;
        string response = "331: User name okay, need password.";
        send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
        return;
    }
    throw bad_sequence_of_commands_exception();
}

void user::login_step2(string pass){
    if (this->current_state == user_entered /*check correct password for this->username in json_config*/)
    {
        log* lg = log::get_instance();
        lg->login_log(this->username);
        this->current_state = authenticated;
        this->password = password;
        string response = "230: User logged in, proceed. Logged out if appropriate.";
        send(this->command_channel, &response[0], MAX_PCKT_SIZE, 0);
        return;
    }
    throw bad_sequence_of_commands_exception();
}

void user::create_data_channel(){
    if((this->data_channel = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) != -1)
        cout << "UDP socket has been created successfully on port " << this->data_channel_port << endl;
    else{
        cout << "socket UDP could not be created on port " << this->data_channel_port << endl;
        close(this->command_channel);
        pthread_exit(NULL);
    }
}

void user::bind_data_channel(struct sockaddr_in* myAddr){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(this->data_channel_port);
    myAddr->sin_addr.s_addr = inet_addr(DEFAULT_IP);

    if(bind(this->data_channel, (struct sockaddr*) myAddr, sizeof(struct sockaddr_in))<0){
        cout << "Binding has failed on port " << this->data_channel_port << endl;
        close(this->command_channel);
        close(this->data_channel);
        pthread_exit(NULL);
    }
    cout << "Binding has been successful on port " << this->data_channel_port << endl;
}