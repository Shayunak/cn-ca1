#ifndef USER_H
#define USER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>  
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Utility/log.h"
#include "file_manager.h"
#include "Utility/command_handler.h"

#define MAX_PCKT_SIZE 1000
#define DEFAULT_IP "127.0.0.1"

enum state {connected, user_entered, authenticated};

class user{
private:
    bool is_admin;
    double remaining_data;
    state current_state;
    std::string username;
    std::string password;
    std::string current_directory;
    int command_channel;
    int data_channel;
    int data_channel_port;
    void check_login();
public:
    user(int _command_channel, int _data_channel_port);
    void start_user();
    void create_data_channel();
    void bind_data_channel(struct sockaddr_in* myAddr);
    void go_to_next_state();
    void reset_state();
    void set_is_admin(bool _is_admin);
    void set_remaining_data(double _remaining_data);
    void set_username(std::string _username);
    void set_password(std::string _password);
    void set_current_directory(std::string _new_directory);
    void send_udp(std::string message);
    void pwd();
    std::string get_directory();
    void make_directory(std::string new_directory);
    void delete_file(std::string file_name);
    void delete_directory(std::string directory_path);
    void ls();
    void change_directory(std::string path);
    void rename_file(std::string old_name, std::string new_name);
    void download_file(std::string file_name);
    void get_help();
    void quit();
    void login_step1(std::string username);
    void login_step2(std::string pass);
};

#endif