#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <string>
#include <fstream>
#include <unistd.h>
#include <stdio.h>
#include <limits>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include "Utility/exceptions.h"
#include <dirent.h>

#define MAX_SIZE 300

class file_manager{

public:
    static file_manager* get_instance();
    std::string get_current_dir();
    std::string get_help();
    void make_directory(std::string current_directory, std::string new_directory);
    void delete_file(std::string current_directory, std::string file_name);
    void delete_directory(std::string current_directory, std::string directory_path);
    std::string ls(std::string current_directory);
    void change_directory(std::string current_directory, std::string path);
    void rename_file(std::string current_directory, std::string new_name, std::string old_name);
    std::ifstream download_file(std::string current_directory, std::string file_name);
private:
    file_manager();
    static file_manager* instance;
    std::string help_text;
    void extract_help_file();
};

#endif