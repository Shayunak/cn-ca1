#include "connection_manager.h"
#include "Utility/command_handler.h"
#include "file_manager.h"
#include "Utility/log.h"

#define SERVER_PORT 8050
#define START_PORT 20000

command_handler* command_handler::cmd_instance = 0;
file_manager* file_manager::instance = 0;
log* log::instance = 0;

int main(){
    connection_manager* manager = new connection_manager(SERVER_PORT, START_PORT);
    manager->run();
}