#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H

#include <vector>
#include <iostream>
#include <string>
#include "../user.h"
#include "exceptions.h"
#include <sstream>

#define QUIT "quit"
#define HELP "help"
#define DOWNLOAD "retr"
#define RENAME "rename"
#define CWD "cwd"
#define LS "ls"
#define USER "user"
#define PASS "pass"
#define PWD "pwd"
#define MKD "mkd"
#define DELETE "dele"

class user;

class command_handler{
private:
    static command_handler* cmd_instance;
    std::vector<std::string> split_command(std::string command);
    void detect_and_run(std::vector<std::string> commands, user* calling_user);
    command_handler(){}
    void handle_login(std::vector<std::string> commands, user* calling_user, int step);
    void handle_deletion(std::vector<std::string> commands, user* calling_user);
public:
    static command_handler* get_instance();
    void run_command(std::string command, user* calling_user);
};

#endif