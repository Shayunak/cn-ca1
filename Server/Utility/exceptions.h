#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

class bad_sequence_of_commands_exception : public std::exception{
    virtual const char* what() const throw(){
        return "503: Bad sequence of commands.";
    }
};

class invalid_username_or_password_exception : public std::exception{
    virtual const char* what() const throw(){
        return "430: Invalid username or password.";
    }
};

class file_unavailable_exception : public std::exception{
    virtual const char* what() const throw(){
        return "550: File unavailable.";
    }
};

class need_account_for_login_exception : public std::exception{
    virtual const char* what() const throw(){
        return "332: Need account for login.";
    }
};

class syntax_error_exception : public std::exception{
    virtual const char* what() const throw(){
        return "501: Syntax error in parameters or arguments.";
    }
};

class other_errors_exception : public std::exception{
    virtual const char* what() const throw(){
        return "500: Error";
    }
};

class out_of_data_exception : public std::exception{
    virtual const char* what() const throw(){
        return "425: Can't open data connection.";
    }
};

#endif