#include "log.h"

using namespace std;

log* log::get_instance(){
    if (instance == nullptr)
        instance = new log();
    return instance;
}

log::log(){

}

void log::write_log(std::string logs){
    file.open(LOGFILE, ios::app);
    file << logs << endl;
    file.close();
}

string log::get_current_time(){
    time_t tt;
    struct tm* ti;
    time(&tt);
    ti = localtime(&tt);
    string current_time = asctime(ti);
    return current_time;
}

void log::login_log(string username){
    string str;
    str = "user " + username + " loged in on " + this->get_current_time();
    this->write_log(str);
}

void log::create_file_log(string username, string file_name){
    string str;
    str = "user " + username + " created file: " + file_name + " on " + this->get_current_time();
    this->write_log(str);
}

void log::delete_file_log(string username, string file_name){
    string str;
    str = "user " + username + " deleted file: " + file_name + " on " + this->get_current_time();
    this->write_log(str);
}

void log::download_file_log(string username, string file_name){
    string str;
    str = "user " + username + " downloaded file: " + file_name + " on " + this->get_current_time();
    this->write_log(str);
}

void log::logout_log(string username){
    string str;
    str = "user " + username + " logged out " + " on " + this->get_current_time();
    this->write_log(str);
}

void log::rename_file_log(string username, string file_old_name, string file_new_name){
    string str;
    str = "user " + username + " renamed file: " + file_old_name + " to " + file_new_name + " on " + this->get_current_time();
    this->write_log(str);
}
// log* log::instance = 0;
// int main(){
//     log* logging = log::get_instance();
//     logging->login_log("ali");
//     logging->create_file_log("mahdi", "ss");
//     logging->close();
    
// }