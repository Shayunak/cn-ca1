#ifndef JSON_CONFIG_READER_H
#define JSON_CONFIG_READER_H

#include <vector>
#include <string>

struct user_info
{
    std::string username;
    std::string password;
    double size;
};

class json_config_reader{

public:
    static json_config_reader* get_instance();
private:
    json_config_reader();
    static json_config_reader* instance;
    void extract_json();

    std::vector<std::string> admin_files;
    std::vector<user_info> users;
};

#endif