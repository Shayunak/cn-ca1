#include "command_handler.h"

using namespace std;

command_handler* command_handler::get_instance(){
    if (cmd_instance == nullptr)
        cmd_instance = new command_handler();
    return cmd_instance;
}

vector<string> command_handler::split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (ss >> part)
        commands.push_back(part);
    
    return commands;
}

void printCommand(vector<string> commands){
    for (int i = 0; i < commands.size(); i++)
    {
        cout << commands[i] << endl;
    }
    
}

void command_handler::handle_login(vector<string> commands, user* calling_user, int step){
    if (commands.size() != 2)
        throw other_errors_exception();
    
    if (step == 1)
        calling_user->login_step1(commands[1]);
    
    if (step == 2)
        calling_user->login_step2(commands[1]);
    
}

void command_handler::handle_deletion(vector<string> commands, user* calling_user){
    if (commands.size() != 3)
        throw other_errors_exception();
    
    if (commands[1] == "-f")
        calling_user->delete_file(commands[2]);
    else{
        if(commands[1] == "-d")
            calling_user->delete_directory(commands[2]);
        else
            throw syntax_error_exception();
    }
}

void command_handler::detect_and_run(vector<string> commands, user* calling_user){
    bool detected = false;

    if (commands[0] == USER){
        this->handle_login(commands, calling_user, 1);
        // printCommand(commands);
        detected = true;
    }
    
    if (commands[0] == PASS){
        this->handle_login(commands, calling_user, 2);
        // printCommand(commands);
        detected = true;
    }
    
    if (commands[0] == PWD)
    {
        if (commands.size() != 1){
            // cout << "got hereeeeeeeeeeeeeeeeeeeee   2 \n";
            printCommand(commands);
            throw other_errors_exception();
        }
        else
            calling_user->pwd();   
            // printCommand(commands);

        detected = true;
    }
    
    if (commands[0] == MKD)
    {
        if (commands.size() != 2)
            throw other_errors_exception();
        else
            calling_user->make_directory(commands[1]);
            // printCommand(commands);

        detected = true;
    }
    
    if (commands[0] == DELETE){
        this->handle_deletion(commands, calling_user);
        // printCommand(commands);
        detected = true;
    }
    
    if (commands[0] == LS)
    {
        if (commands.size() != 1)
            throw other_errors_exception();
        else
            calling_user->ls();
            // printCommand(commands);
        detected = true;
    }
    
    if (commands[0] == CWD)
    {
        if (commands.size() != 2)
            throw other_errors_exception();
        else
            calling_user->change_directory(commands[1]);
            // printCommand(commands);
        detected = true;
    }
    
    if (commands[0] == RENAME)
    {
        if (commands.size() != 3)
            throw other_errors_exception();
        else
            calling_user->rename_file(commands[1], commands[2]);
            // printCommand(commands);
        detected = true;
    }

    if (commands[0] == DOWNLOAD)
    {
        if (commands.size() != 2)
            throw other_errors_exception();
        else
            calling_user->download_file(commands[1]);
            // printCommand(commands);
        detected = true;
    }

    if (commands[0] == HELP)
    {
        if (commands.size() != 1)
            throw other_errors_exception();
        else
            calling_user->get_help();
            // printCommand(commands);
        detected = true;
    }

    if (commands[0] == QUIT)
    {
        if (commands.size() != 1)
            throw other_errors_exception();
        else
            calling_user->quit();
            // printCommand(commands);
        detected = true;
    }

    if (!detected){
        // cout << "got hereeeeeeeee!\n";
        throw other_errors_exception();
    }
    
}

void command_handler::run_command(std::string command, user* calling_user){
    vector<string> commands = this->split_command(command);
    this->detect_and_run(commands, calling_user);
    // this->detect_and_run(commands, nullptr);
    // printCommand(commands);
}

// command_handler* command_handler::cmd_instance = 0;
// int main(){
//     command_handler* cmd = command_handler::get_instance();
//     string input;
//     input = "salma dsd asada!";
//     while (input != QUIT)
//     {
//         getline(cin, input);
//         try
//         {
//             cmd->run_command(input, nullptr);
//         }
//         catch(const exception& e)
//         {
//             cout << e.what() << '\n';
//         }
//     }
//     return 0;
// }