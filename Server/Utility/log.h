#ifndef LOG_H
#define LOG_H

#include <string>
#include <time.h>
#include <sstream>
#include <fstream>
#include <time.h>
#include <iostream>

#define LOGFILE "log.txt"

class log{
public:
    static log* get_instance();
    void write_log(std::string logs);
    void login_log(std::string username);
    void create_file_log(std::string username, std::string file_name);
    void delete_file_log(std::string username, std::string file_name);
    void download_file_log(std::string username, std::string file_name);
    void rename_file_log(std::string username, std::string file_old_name, std::string file_new_name);
    void logout_log(std::string username);
    std::string get_current_time();
private:
    //lock
    log();
    static log* instance;
    std::fstream file;
};

#endif