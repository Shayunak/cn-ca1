#include "connection_manager.h"

using namespace std;

connection_manager::connection_manager(int _port, int _last_data_channel_port){
    this->port = _port;
    this->last_data_channel_port = _last_data_channel_port;
    this->initialize_server();
}

void connection_manager::create_socket(){
    if((this->listening_sock_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) != -1)
        cout << "TCP socket has been created successfully!\n";
    else{
        cout << "TCP socket could not be created!\n";
        exit(EXIT_FAILURE);
    }
}

void connection_manager::binding(struct sockaddr_in* myAddr){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(this->port);
    myAddr->sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(this->listening_sock_fd , (struct sockaddr*) myAddr , sizeof(struct sockaddr_in) ) < 0 ){
        cout << "Binding to Port Failed!\n";
        close(this->listening_sock_fd);
        exit(EXIT_FAILURE);
    }
    cout << "Server is successfully bound to port!\n";
}

void connection_manager::listening(){
    if(listen(this->listening_sock_fd , MAX_LISTEN_Q) < 0){
        cout << "Listen Failed!\n";
        close(this->listening_sock_fd);
        exit(EXIT_FAILURE);
    }
    cout << "Server is Listening...\n";
}

void connection_manager::initialize_server(){
    struct sockaddr_in myAddr;
    this->create_socket();
    this->binding(&myAddr);
    this->listening();
}

int connection_manager::next_data_channel_port(){
    int data_channel = last_data_channel_port;
    last_data_channel_port++;
    return data_channel;
}

void* connection_manager::create_new_user(void* thread){
    user* new_user = (user*) thread;
    new_user->start_user();
    return nullptr;
}

void connection_manager::run(){
    
    while (1)
    {
        struct sockaddr client_addr;
        pthread_t thread;
        int lenAddr = sizeof(client_addr);
        int new_client_socket = accept(this->listening_sock_fd , &client_addr , (socklen_t*) &lenAddr);
        user* new_user = new user(new_client_socket, this->next_data_channel_port());
        pthread_create(&thread, NULL, create_new_user, (void*)new_user);
    }
}