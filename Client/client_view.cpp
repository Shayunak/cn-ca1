#include "client_view.h"

using namespace std;

client_view::client_view(){
}

string client_view::get_input(){
    string input;
    getline(cin, input);
    return input;
}

void client_view::show_output(string output){
    cout << output << endl;
}