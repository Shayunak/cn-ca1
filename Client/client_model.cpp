#include <iostream>
#include <cstdlib>
#include "client_model.h"

using namespace std;

client_model::client_model(int _port){
    this->server_port = _port;
    this->command_channel = 0;
    this->data_channel = 0;
    this->view = new client_view();
    this->initialize_server_connection();
    this->initialize_data_channel_connection();
}

void client_model::create_command_channel(){
    if((this->command_channel = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) != -1)
        view->show_output("TCP socket has been created successfully!");
    else{
        view->show_output("TCP socket could not be created!");
        exit(EXIT_FAILURE);
    }
}

void client_model::tcp_connect_server(struct sockaddr_in* servAddr){
    servAddr->sin_family = AF_INET;
    servAddr->sin_port = htons(this->server_port);
    servAddr->sin_addr.s_addr = inet_addr(DEFAULT_IP);
    if(connect(this->command_channel , (struct sockaddr*) servAddr , sizeof(struct sockaddr_in) ) < 0){
        view->show_output("Connection Blocked By Server!");
        close(this->command_channel);
        exit(EXIT_FAILURE);
    }
    view->show_output("Successfully connected to the server!");
}

void client_model::initialize_server_connection(){
    struct sockaddr_in servAddr;
    create_command_channel();

    view->show_output("Wait for the server to respond....");
    tcp_connect_server(&servAddr);
}

void client_model::create_data_channel(){
    if((this->data_channel = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) != -1)
        view->show_output("UDP socket has been created successfully!");
    else{
        view->show_output("socket UDP could not be created!");
        close(this->command_channel);
        exit(EXIT_FAILURE);
    }
}

void client_model::data_channel_binding(struct sockaddr_in* myAddr){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(this->data_channel_port);
    myAddr->sin_addr.s_addr = inet_addr(DEFAULT_IP);

    if(bind(this->data_channel, (struct sockaddr*) myAddr, sizeof(struct sockaddr_in))<0){
        view->show_output("Binding has failed");
        close(this->command_channel);
        close(this->data_channel);
        exit(EXIT_FAILURE);
    }
    view->show_output("Binding has been successful");
}

void client_model::initialize_data_channel_connection(){
    struct sockaddr_in myAddr;
    this->data_channel_port = stoi(receive_response() );
    this->create_data_channel();
    this->data_channel_binding(&myAddr);
}

void client_model::exit_program(){
    close(this->command_channel);
    if(this->data_channel != 0)
        close(this->data_channel);
    exit(EXIT_FAILURE);
}

void client_model::run(){
    while(true){
        string command = view->get_input();
        if(command == "exit")
            this->exit_program();
        send_command(command);
        string response = receive_response();
        view->show_output(response);
        this->check_data_channel(response);
    }
}

void client_model::send_command(string input){
    send(this->command_channel, &input[0], input.length(), 0);
}

string client_model::receive_response(){
    char response[MAX_PCKT_SIZE_COMMAND];
    memset(response, 0, MAX_PCKT_SIZE_COMMAND);
    recv(this->command_channel, response, MAX_PCKT_SIZE_COMMAND, 0);
    return string(response);
}

bool client_model::checkIf226(string message){
    if(message.length() < 5)
        return false;
    if(message[0] == '2' && message[1] == '2' && message[2] == '6')
        return true;
    return false;
}

bool client_model::checkIfLs(string message){
    if(message[5] == 'L')
        return true;
    return false;
}

string client_model::recieve_udp(){
    char list_of_files[MAX_PCKT_SIZE_COMMAND];
    struct sockaddr_in myAddr;
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(this->data_channel_port);
    myAddr.sin_addr.s_addr = inet_addr(INADDR_ANY);
    int addrLen = sizeof(myAddr);

    memset(list_of_files, 0, MAX_PCKT_SIZE_COMMAND);
    recvfrom(this->data_channel, list_of_files, MAX_PCKT_SIZE_COMMAND, 0, (struct sockaddr*) &myAddr , (socklen_t*) &addrLen);
    // cout << "sfd: " << list_of_files << endl;
    return string(list_of_files);
}

void client_model::check_data_channel(string response){
    if(!checkIf226(response) )
        return;
    if(checkIfLs(response) )
        view->show_output(recieve_udp() );
}