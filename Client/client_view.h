#ifndef CLIENT_VIEW_H
#define CLIENT_VIEW_H

#include <string>
#include <iostream>

class client_view{
public:
    client_view();
    std::string get_input();
    void show_output(std::string output);
};

#endif