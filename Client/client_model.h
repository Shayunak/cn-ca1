#ifndef CLIENT_MODEL_H
#define CLIENT_MODEL_H

#include<string>
#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>  
#include "client_view.h"

#define DEFAULT_IP "127.0.0.1"
#define MAX_PCKT_SIZE_COMMAND 1000

class client_model{
public:
    client_model(int _port);
    void run();
private:
    //command channel
    void initialize_server_connection();
    void create_command_channel();
    void tcp_connect_server(struct sockaddr_in* servAddr);
    // data channel
    void initialize_data_channel_connection();
    void create_data_channel();
    void data_channel_binding(struct sockaddr_in* myAddr);
    void check_data_channel(std::string response);
    bool checkIf226(std::string message);
    bool checkIfLs(std::string message);
    std::string recieve_udp();
    /// send/recieve to/from server
    void send_command(std::string input);
    std::string receive_response();
    ///exit
    void exit_program();
    /// fields
    int server_port;
    int command_channel;
    int data_channel;
    int data_channel_port;
    sockaddr* client_address;
    client_view* view;
};

#endif